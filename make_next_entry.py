ENTRIES_DIR = "entries"

from os import listdir

entries = listdir(ENTRIES_DIR)

winner = -1
temp = 0

for entry in entries:
    temp = int(entry.split('.')[0], 16)

    if temp > winner:
        winner = temp

winner += 1

winner = hex(winner).upper()

new_entry = ENTRIES_DIR + "/" + str(winner) + ".md"

fp = open(new_entry, "w")
fp.close()
